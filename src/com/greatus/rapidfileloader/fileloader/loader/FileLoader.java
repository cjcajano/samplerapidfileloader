/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.fileloader.loader;

import java.io.File;

/**
 *
 * @author donro
 */
public interface FileLoader
{
	void loadFileFrom(File directory);
}

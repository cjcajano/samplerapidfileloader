package com.greatus.rapidfileloader.fileloader.loader;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.greatus.rapidfileloader.fileloader.customthreadpool.EmployeeRepositoryService;
import com.greatus.rapidfileloader.fileloader.customthreadpool.FileLoaderService;
import com.greatus.rapidfileloader.fileloader.dao.EmployeeCache;

public class CheckerThread extends Thread {
	
	private static final Logger LOG = Logger.getLogger(CheckerThread.class.getName());
	private static final double MILLIS_IN_SECONDS = 1000.00;
	
	private int expectedTotalRecords;
	private FileLoaderService fileLoadService;
	private EmployeeRepositoryService employeeRepositoryService;
	
	public CheckerThread(
			final int expectedTotalRecords,  
			final FileLoaderService fileLoadService, 
			final EmployeeRepositoryService employeeRepositoryService
	){
		this.expectedTotalRecords = expectedTotalRecords;
		this.fileLoadService = fileLoadService;
		this.employeeRepositoryService = employeeRepositoryService;
	}
	
	public void run(){
		boolean timeToStop = false;
		long startSeconds = System.currentTimeMillis();
		
		while(!timeToStop){
			timeToStop = false;
			
			if(expectedTotalRecords == EmployeeCache.getCurrentCache().size()){
				timeToStop = true;
			}
			
			if(timeToStop){
				
				fileLoadService.shutDown();
				employeeRepositoryService.shutDown();
				LOG.log(Level.INFO, "Number of actual records persisted: {0}",EmployeeCache.getCurrentCache().size());
				
				long endSeconds = System.currentTimeMillis(); 
				LOG.log(Level.INFO, "START TIME {0} - END TIME: {1}, TOTAL RUN TIME: {2}", new Object[]{startSeconds, endSeconds, (endSeconds - startSeconds)/MILLIS_IN_SECONDS});
			}
		}
	}
}

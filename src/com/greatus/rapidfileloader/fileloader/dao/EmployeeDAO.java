/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.fileloader.dao;

import com.greatus.rapidfileloader.model.Employee;

/**
 *
 * @author donro
 */
public interface EmployeeDAO
{
	void saveEmployee(Employee employee);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.fileloader.dao;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.greatus.rapidfileloader.model.Employee;

/**
 *
 * @author donro
 */
public class EmployeeDAOImpl implements EmployeeDAO
{
	private static final Logger LOG = Logger.getLogger(EmployeeDAOImpl.class.getName());

	@Override
	public void saveEmployee(Employee employee)
	{
		Long sleepTime = 200L;
		try
		{
			LOG.log(Level.INFO, "simulated saving of employee for {1} millisecond(s) transaction: {0}",new Object[]{employee,sleepTime});
			
			Thread.sleep(sleepTime);
			EmployeeCache.getCurrentCache().add(employee);
		}
		catch (InterruptedException ex)
		{
			LOG.log(Level.WARNING, ex.getMessage(),ex);
		}
	}

	
}

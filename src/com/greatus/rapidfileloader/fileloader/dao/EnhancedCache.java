package com.greatus.rapidfileloader.fileloader.dao;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public abstract class EnhancedCache<K, V> {
	protected Map<K,V> cache;
	
	EnhancedCache(){
		cache = new ConcurrentSkipListMap<K, V>(setupComparator());
	}
	
	protected abstract Comparator<K> setupComparator();
	
	public abstract void add(V record);
	
	public abstract boolean exists(V record); 
	
	public int size(){
		return cache.size();
	}
}
